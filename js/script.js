let userNumber;
do {
  userNumber = +prompt("Enter you number:");
} while (!Number.isInteger(userNumber));

if (userNumber < 5) {
  console.log("Sorry, no numbers");
} else {
  for (let i = 0; i <= userNumber; i++) {
    if (i % 5 === 0) {
      console.log(i);
    }
  }
}
